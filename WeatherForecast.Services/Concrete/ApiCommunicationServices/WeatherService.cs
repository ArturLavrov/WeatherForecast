﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherForecast.Services.ApiCommunicationServices;

namespace WeatherForecast.Services
{
    public class WeatherService : IWeatherService
    {
        private IHttpCoreService _httpCoreService;
        public WeatherService(IHttpCoreService httpCoreService)
        {
            _httpCoreService = httpCoreService;
        }
        
        public async Task<string> GetCurrentWeather(string city, int days = 7)
        {
            string url = _httpCoreService.CreateUrl(city, days, ConstantsConfig.CurrentWeatherPath);
            var result = await _httpCoreService.GetAsynk(url);
            return result;
        }

        public async Task<string> GetWeatherForecast(string city, int days=7)
        {
            var url = _httpCoreService.CreateUrl(city,days,ConstantsConfig.ForecastPath);
            var result = await _httpCoreService.GetAsynk(url);
            return result;
        }
    }
}
