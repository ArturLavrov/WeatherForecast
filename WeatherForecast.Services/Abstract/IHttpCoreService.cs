﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherForecast.Services
{
    public interface IHttpCoreService
    {
        Task<string> GetAsynk(string url);
        string CreateUrl(string city, int days,string path);
    }
}
