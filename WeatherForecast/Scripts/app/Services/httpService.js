﻿weatherForecast.factory('httpGET', ['$http', function ($http) {
    return function (location,serverUrl) {
        return $http.get(serverUrl, {
            params: { location: location }
        }).success(function (data) {

        })
       .error(function (err) {
          console.log(err);
        });
    };
}]);