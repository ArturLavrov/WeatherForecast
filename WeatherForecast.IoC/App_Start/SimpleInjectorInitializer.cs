[assembly: WebActivator.PostApplicationStartMethod(typeof(WeatherForecast.IoC.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace WeatherForecast.IoC.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;
    using global::SimpleInjector;
    using global::SimpleInjector.Integration.Web.Mvc;
    using global::SimpleInjector.Integration.Web;
    using Services;
    using Services.ApiCommunicationServices;

    public static class SimpleInjectorInitializer
    {
        //Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
            // For instance:
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);
            container.Register<IWeatherService, WeatherService>(Lifestyle.Transient);
            container.Register<IHttpCoreService, HttpCoreService>(Lifestyle.Transient);
        }
    }
}